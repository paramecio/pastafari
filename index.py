from paramecio.wsgiapp import app
from paramecio.citoplasma.urls import redirect
from paramecio.citoplasma.adminutils import make_admin_url
from settings import config

@app.get('/pastafari')
def home():
    
    redirect(make_admin_url('pastafari/dashboard'))

if config.default_module=="pastafari":

    home = route("/")(home)

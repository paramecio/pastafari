#!/usr/bin/env python3

import traceback, sys, time
from paramecio.citoplasma.mtemplates import env_theme, PTemplate
from paramecio.citoplasma.i18n import load_lang, I18n
from paramecio.citoplasma.urls import make_url, add_get_parameters, redirect
from paramecio.citoplasma.adminutils import get_menu, get_language, check_login
from paramecio.citoplasma.sessions import get_session
from paramecio.citoplasma.lists import SimpleList
from bottle import route, get,post,response,request
from settings import config
from settings import config_admin
from paramecio.citoplasma.httputils import GetPostFiles, filter_ajax
from paramecio.cromosoma.webmodel import WebModel
from modules.pastafari.models.tasks import Task, LogTask
from modules.pastafari.models.servers import Server
from modules.pastafari.libraries.configtask import config_task
from paramecio.wsgiapp import app
from bottle import request
import requests
import json

# Get json data for a see progress in task in a server

pastafari_folder='pastafari'

if hasattr(config, 'pastafari_folder'):
    pastafari_folder=config.pastafari_folder

num_tasks=10

if hasattr(config, 'num_tasks'):
    num_tasks=config.num_tasks

env=env_theme(__file__)

t=PTemplate(env)

@app.route('/'+pastafari_folder+'/showprogress/tasks')
@app.post('/'+pastafari_folder+'/showprogress/tasks')
def gettasks():
    
    s=get_session()
    
    if check_login() or s.get('get_progress', 0)>0:
    
        conn=WebModel.connection()

        getpostfiles=GetPostFiles()

        getpostfiles.obtain_get()
        
        getpostfiles.get['op']=getpostfiles.get.get('op', '')
        getpostfiles.get['task_id']=getpostfiles.get.get('task_id', '0')
        getpostfiles.get['position']=getpostfiles.get.get('position', '0')
        getpostfiles.get['server']=getpostfiles.get.get('server', '')
        
        if getpostfiles.get['task_id']=='0':
            
            s['get_progress']=s.get('get_progress', 0)
            getpostfiles.get['task_id']=str(s['get_progress'])
            
            
        
        try:
        
            task_id=int(getpostfiles.get['task_id'])
        except:
            task_id=0
            
        try:
            position=int(getpostfiles.get['position'])
        except:
            position=0
        
        task=Task(conn)
        logtask=LogTask(conn)
        
        arr_task=task.select_a_row(task_id)
        
        arr_rows={'wait': 1}
        
        if arr_task:
            
            logtask.set_limit([position, 20])
                
            logtask.set_order({'id': 0})
            
            logtask.conditions=['WHERE task_id=%s', [task_id]]
            
            if getpostfiles.get['server']!='':
                logtask.conditions=['WHERE task_id=%s and logtask.server=%s', [task_id, getpostfiles.get['server']]]
                
            #logtask.set_limit([position, 1])
            
            #arr_row=logtask.select_a_row_where([], 1, position)
            
            logtask.yes_reset_conditions=False
            
            #c=logtask.select_count()
            
            #if c>0:
            c=0
                
            arr_rows=[]
            
            with logtask.select([], True) as cursor:            
                for arr_row in cursor:
                    arr_rows.append(arr_row)
                    c+=1
            
            if c==0:
                arr_rows=[]
                """
                with logtask.set_conditions('WHERE task_id=%s and logtask.error=1 and logtask.status=1', [task_id]).select([], True) as cursor:            
                    for arr_row in cursor:
                        arr_rows.append(arr_row)
                """
                
                c_error=logtask.set_conditions('WHERE task_id=%s and logtask.error=%s and logtask.status=%s', [task_id, 1, 1]).select_count()
                
                if c_error==0:
                    
                    arr_rows={'wait': 1}
                else:
                    logtask.limit=''
                    
                    with logtask.set_conditions('WHERE task_id=%s and logtask.error=%s and logtask.status=%s', [task_id, 1, 1]).select([], True) as cursor:            
                        for arr_row in cursor:
                            arr_rows.append(arr_row)
                    
        else:
            
            arr_rows={'task_id': task_id, 'progress': 100, 'message': 'Error: no exists task', 'error': 1, 'status': 1}
            
        conn.close()
        
        return filter_ajax(arr_rows)

        
        
    else:

        redirect(make_url(config.admin_folder))

@app.get('/'+pastafari_folder+'/getservers/<task_id:int>/<position:int>')
def getservers(task_id, position):
    
    conn=WebModel.connection()
    
    s=get_session()

    getpostfiles=GetPostFiles()

    getpostfiles.obtain_get()

    s['get_progress']=s.get('get_progress', 0)

    if check_login() or s['get_progress']==task_id and  s['get_progress']!=0:

        task=Task(conn)
        logtask=LogTask(conn)
        server=Server(conn)
        
        arr_task=task.select_a_row(task_id)
        
        server.set_conditions('WHERE ip IN (select DISTINCT server from logtask where task_id=%s)', [task_id])
        
        server.set_limit([position, num_tasks])
        
        arr_server=server.select_to_array(['hostname', 'ip'])
        
        response.set_header('Content-type', 'text/plain')
        
        if arr_server:
            conn.close()
            return filter_ajax({'servers': arr_server, 'error': 0})
            
        else:
            
            logtask.set_conditions('where task_id=%s and server=""', [task_id])
            
            logtask.set_order({'id': 1})
            
            arr_tasklog=logtask.select_a_row_where([], True)
            
            if arr_tasklog:
                
                if arr_tasklog['error']==1:
                    conn.close()        
                    return arr_tasklog
                else:
                    conn.close()
                    return filter_ajax({'error': 0, 'servers': []})
                    
            else:
                conn.close()
                return filter_ajax({'error': 0, 'servers': []})
                
            
            pass
        

    else:
        conn.close()
        return filter_ajax({})


@app.post('/'+pastafari_folder+'/getprogress/<task_id:int>')
def getprogress(task_id):
    
    conn=WebModel.connection()
    
    s=get_session()

    s['get_progress']=s.get('get_progress', 0)

    if check_login() or s['get_progress']==task_id and  s['get_progress']!=0:
        
        getpost=GetPostFiles() 
        
        getpost.obtain_post([], True)

        task=Task(conn)
        logtask=LogTask(conn)
        server=Server(conn)
        
        arr_task=task.select_a_row(task_id)
        
        try:
            
            servers=json.loads(getpost.post['servers'])
            
        except:
            
            servers={}
        
        #for ip in servers:
        
        if len(servers)>0:
        
            logtask.set_order({'id': 1})
            
            logtask.set_conditions('WHERE task_id=%s and status=1 and error=1 and server=""', [task_id])
            
            c_error=logtask.select_count()
            
            if c_error==0:
            
                logtask.set_order({'id': 1})
                
                checked_servers=logtask.check_in_list_str('server', servers)

                logtask.set_conditions('WHERE task_id=%s and status=1 and server IN '+checked_servers+' and server!=""', [task_id])
                
                arr_log=logtask.select_to_array(['status', 'error', 'server'])
                
                logtask.set_order({'id': 1})
                
                logtask.set_conditions('WHERE task_id=%s and status=0 and server NOT IN '+checked_servers+' and server!=""', [task_id])
                
                arr_log2=logtask.select_to_array(['status', 'error', 'server'])
                
                arr_log=arr_log2+arr_log
                
                #response.set_header('Content-type', 'text/plain')
                
                #return json.dumps(arr_log)
                
            else:
                
                arr_log=[]
                
                for server in servers:
                    
                    arr_log.append({'status':1, 'error':1, 'server': server})
                    
            response.set_header('Content-type', 'text/plain')
            
            conn.close()
            
            return filter_ajax(arr_log)
            
        response.set_header('Content-type', 'text/plain')    
        
        arr_log=[]
        
        conn.close()        
        
        return filter_ajax(arr_log)
                
                

    else:
        conn.close()
        return filter_ajax({})


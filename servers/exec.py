#!/usr/bin/env python3

import argparse
import json
from paramecio.cromosoma.webmodel import WebModel
from modules.pastafari.models import servers, tasks
from modules.pastafari.libraries.task import Task
from settings import config
from modules.pastafari.libraries.configtask import config_task
from multiprocessing.pool import Pool
import importlib
from paramecio.cromosoma.databases.sqlalchemy import SqlClass
SqlClass.disable_pool=True

def start():
    
    parser = argparse.ArgumentParser(description='A daemon used for make a task. The results are saved in a sql database using task class')
    parser.add_argument('--task_id', help='The task to execute', required=True)

    args = parser.parse_args()

    task_id=int(args.task_id)

    conn=WebModel.connection()

    task_model=tasks.HostTask(conn)

    logtask=tasks.LogHostTask(conn)
    logtask.safe_query()

    arr_task=task_model.select_a_row(task_id)
    
    arr_task['conn']=conn
    
    if arr_task:
        
        taskmod=importlib.import_module(arr_task.get('task_module', ''))
        
        args=json.loads(arr_task['args'])
        
        if taskmod.execute_script(args):
            logtask.insert({'task_id': task_id, 'progress': 100, 'message': 'Task done', 'error': 0, 'status': 1})
            
        else:
            logtask.insert({'task_id': task_id, 'progress': 100, 'message': 'Error executing '+executable+': '+str(line)+"\n"+str(line_error), 'error': 1, 'status': 1})            
            
    
    conn.close()

if __name__=='__main__':
    start()

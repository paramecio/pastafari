#!/usr/bin/env python3

import sys, os
sys.path.insert(0, os.path.realpath(os.path.dirname(__file__))+'/../../../')
import json
from paramecio.cromosoma.webmodel import WebModel
from modules.pastafari.models import servers, tasks
from modules.pastafari.libraries.task import Task
from settings import config
from modules.pastafari.libraries.configtask import config_task
from multiprocessing.pool import Pool
import importlib
from paramecio.cromosoma.databases.sqlalchemy import SqlClass
#SqlClass.disable_pool=True

def send_pastafari_task(task_id, ip):

    conn=WebModel.connection()

    task_model=tasks.Task(conn)

    logtask=tasks.LogTask(conn)

    arr_task=task_model.select_a_row(task_id)
    
    if arr_task:
        
        arr_task['server']=ip
        
        if (arr_task['user']!='' or arr_task['password']!='') and arr_task['path']!='':
            
            config_task.remote_user=arr_task['user']
            config_task.remote_password=arr_task['password']
            config_task.remote_path=arr_task['path']
            
            task_model.create_forms()
            task_model.reset_require()
            
            task_model.set_conditions('WHERE id=%s', [task_id])
            
            task_model.update({'password': ''})

        server_model=servers.Server(conn)        
                
        task=generate_task(arr_task, conn, task_id)

        arr_server=server_model.set_conditions('where ip=%s', [arr_task['server']]).select_a_row_where()
        
        task.os_server=arr_server['os_codename']
        
        task.exec()

    conn.close()

    exit(1)

def generate_task(arr_task, conn, task_id):

    
    #taskssh.config=taskset.get('config', taskssh.config)
    #arr_task={k: v for k, v in arr_task.items() if v is not "" and v is not "[]"}
    
    taskmod=importlib.import_module(arr_task.get('task_module', ''))
    
    taskssh=taskmod.TaskServer(arr_task['server'], conn, task_id)
    
    if arr_task['name_task']!='':
        taskssh.name_task=arr_task['name_task']

    if arr_task['codename_task']!='':
        taskssh.codename_task=arr_task['codename_task']

    if arr_task['description_task']!='':
        taskssh.description_task=arr_task['description_task']

    """
    if arr_task['os_server']!='':
        taskssh.os_server=arr_task['os_server']
    """

    if arr_task['files']!='[]':
        taskssh.files=json.loads(arr_task['files'])

    if arr_task['commands_to_execute']!='[]':
        taskssh.commands_to_execute=json.loads(arr_task['commands_to_execute'])

    if arr_task['delete_files']!='[]':
        taskssh.delete_files=json.loads(arr_task['delete_files'])

    if arr_task['delete_directories']!='[]':
        taskssh.delete_directories=json.loads(arr_task['delete_directories'])

    """
    if arr_task['one_time']!=0:
        taskssh.one_time=True

    if arr_task['version']!='':
        taskssh.version=arr_task['version']
    """
    
    taskssh.extra_data=json.loads(arr_task['extra_data'])
    
    return taskssh

if __name__=='__main__':
    start()

#from celery import Celery
import sys, os

sys.path.insert(0, os.path.realpath(os.path.dirname(__file__))+'/../../../')

from settings import config
from paramecio.cromosoma.webmodel import WebModel
from paramecio.citoplasma.i18n import I18n
from modules.pastafari.libraries.task import Task
from modules.pastafari.models import tasks
from copy import copy
from importlib import import_module

broker='redis://localhost:6379/0'

if hasattr(config, 'broker'):
    broker=config.broker
    

backend='redis://localhost:6379/0'

if hasattr(config, 'backend'):
    backend=config.backend

#app = Celery('tasks', broker=broker, backend=backend)

#@app.task
def make_task(server, taskmodule, arr_task={}, task_id=None):
    
    conn=WebModel.connection()
    
    task=tasks.Task(conn)
    
    url_return=''
    
    if taskset.get('url_return', '')!='':
        url_return=taskset['url_return']

    taskmod=import_module(taskmodule)
    
    taskssh=taskmod.TaskServer(server, conn, task_id)
    
    if 'user' in arr_task:
        taskssh.config.remote_user=arr_task['user']
        
    if 'password' in arr_task:
        taskssh.config.remote_password=arr_task['password']
        
    if 'path' in arr_task:
        taskssh.config.remote_path=arr_task['path']
    
    if 'name_task' in arr_task:
        taskssh.name_task=arr_task['name_task']

    if 'codename_task' in arr_task:
        taskssh.codename_task=arr_task['codename_task']

    if 'description_task' in arr_task:
        taskssh.description_task=arr_task['description_task']

    if 'os_server' in arr_task:
        taskssh.os_server=arr_task['os_server']

    if 'files' in arr_task:
        taskssh.files=json.loads(arr_task['files'])

    if 'commands_to_execute' in arr_task:
        taskssh.commands_to_execute=json.loads(arr_task['commands_to_execute'])

    if 'delete_files' in arr_task:
        taskssh.delete_files=json.loads(arr_task['delete_files'])

    if 'delete_directories' in arr_task:
        taskssh.delete_directories=json.loads(arr_task['delete_directories'])

    if 'one_time' in arr_task:
        taskssh.one_time=arr_task['one_time']

    if 'version' in arr_task:
        taskssh.version=arr_task['version']
        
    if 'simultaneous' in arr_task:
        taskssh.simultaneous=arr_task['simultaneous']

    if 'extra_data' in arr_task:
        taskssh.extra_data=arr_task['extra_data']
    
    #task.insert({'name_task': taskssh.name_task,'description_task': taskssh.description_task, 'url_return': url_return, 'task_id': task_id})
    
    #taskssh.exec()
    
    conn.close()
    



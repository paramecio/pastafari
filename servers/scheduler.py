from gevent import monkey; monkey.patch_all()
from gevent.subprocess import Popen, PIPE
#from multiprocessing import Process
import argparse
import uuid
import gevent, traceback, sys, time
from bottle import route, run, default_app
from importlib import import_module
from os.path import isfile
from settings import config
from paramecio.citoplasma.i18n import I18n
from paramecio.cromosoma.webmodel import WebModel
from modules.pastafari.models import servers, tasks
from modules.pastafari.libraries.task import Task
from modules.pastafari.libraries.configtask import config_task

# For deploy with uwsgi;  uwsgi --gevent 100 --http-socket :9090 --wsgi-file scheduler.py 

pastafari_scripts='./scripts'
pastafari_host='127.0.0.1'
pastafari_port=1337
pastafari_py_command='python3'

#parser.add_argument('--port', help='The port where the task server is executed', required=True)

if hasattr(config, 'pastafari_scripts'):
    pastafari_scripts=config.pastafari_scripts

if hasattr(config, 'pastafari_port'):
    pastafari_port=config.pastafari_port
    
if hasattr(config, 'pastafari_host'):
    pastafari_host=config.pastafari_host

if hasattr(config, 'pastafari_py_command'):
    pastafari_py_command=config.pastafari_py_command

def execute_script(task_id, executable='launcher.py'):
    
    args=['%s %s --task_id=%i' % (pastafari_py_command, executable, task_id)]
    
    with Popen(args, bufsize=None, executable=None, stdin=None, stdout=PIPE, stderr=PIPE, preexec_fn=None, shell=True, cwd=None, env=None, universal_newlines=False, startupinfo=None, creationflags=0, threadpool=None) as proc:
        
        proc.wait()
        
        return_value=proc.returncode

        if return_value>0:
            connection=WebModel.connection()
            
            if executable=='launcher.py':
            
                logtask=tasks.LogTask(connection)
                task=tasks.Task(connection)
            else:
                logtask=tasks.HostLogTask(connection)
                task=tasks.HostTask(connection)                
            
            logtask.create_forms()
            task.create_forms()
            
            
            line=proc.stdout.read().decode('utf-8')
            line_error=proc.stderr.read().decode('utf-8')
            logtask.insert({'task_id': task_id, 'progress': 100, 'message': 'Error executing '+executable+': '+str(line)+"\n"+str(line_error), 'error': 1, 'status': 1})
            #Status die
            task.set_conditions('where id=%s', [task_id])
            task.reset_require()
            task.update({'status': 1, 'error': 1})
    
            connection.close()

@route('/')
def home():
    
    response={'error': 1, 'code_error': 1, 'message': 'Nothing to see here...', 'progress' : 100}

    return response

@route('/exec/<api_key>/<task_id:int>/<type_task:int>')
@route('/exec/<api_key>/<task_id:int>')
def index(api_key, task_id, type_task=0):
    
    connection=WebModel.connection()
    
    # Get the task to execute.
    executable='launcher.py'
    
    if api_key==config_task.api_key:
    
    
        if not type_task:
            task=tasks.Task(connection)
            logtask=tasks.LogTask(connection)
        else:
            logtask=tasks.HostLogTask(connection)
            task=tasks.HostTask(connection)    
            executable='exec.py'                    
            
        logtask.create_forms()

        arr_task=task.select_a_row(task_id)
        
        if arr_task:
        
        # Add to queue

            greenlet = gevent.spawn( execute_script, task_id, executable)

            # Close the connection

            response={'error': 0, 'code_error': 0, 'message': 'Begin task with id '+str(task_id), 'progress' : 0}
            connection.close()
            return response
        else:
            
            response={'error': 1, 'code_error': 1, 'message': 'Doesnt exists task with id '+str(task_id), 'progress' : 100, 'status': 1}
            connection.close()        
            return response
        
    else:
        
        response={'error': 1, 'code_error': 1, 'message': 'No permission for make tasks', 'progress' : 100, 'status': 1}
        connection.close()        
        return response
        #logtask.insert({})

"""
@route('/exec_script/<api_key>/<task_id:int>')
def index(api_key, task_id):
    
    connection=WebModel.connection()
    
    # Get the task to execute.
    
    if api_key==config_task.api_key:
    
        task=tasks.HostTask(connection)
        logtask=tasks.LogHostTask(connection)
        logtask.safe_query()

        arr_task=task.select_a_row(task_id)
        
        if arr_task:
        
        # Add to queue

            greenlet = gevent.spawn( execute_script, task_id, 'exec.py')

            # Close the connection

            response={'error': 0, 'code_error': 0, 'message': 'Begin task with id '+str(task_id), 'progress' : 0}
            connection.close()
            return response
        else:
            
            response={'error': 1, 'code_error': 1, 'message': 'Doesnt exists task with id '+str(task_id), 'progress' : 100, 'status': 1}
            connection.close()        
            return response
        
    else:
        
        response={'error': 1, 'code_error': 1, 'message': 'No permission for make tasks', 'progress' : 100, 'status': 1}
        connection.close()        
        return response
"""

app = application  = default_app()

# Init the task servers

def run_app(app):
    run(app=app, host=pastafari_host, port=pastafari_port, debug=config.debug, server='gevent', reloader=config.reloader)

if __name__=='__main__':

    run_app(app)


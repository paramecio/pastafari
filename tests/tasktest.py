#!/usr/bin/env python3 

import unittest, os, sys

sys.path.insert(0, os.path.realpath(os.path.dirname(__file__))+'/../../../')

from modules.pastafari.libraries.task import Task
from modules.pastafari.models.tasks import Task as TaskModel
from paramecio.cromosoma.webmodel import WebModel
from settings import config

server_test='192.168.2.5'

if hasattr(config, 'server_test'):
    server_test=config.server_test

class TestTask(unittest.TestCase):
    
    def test_task(self):
        # You can change defined server_test variable in settings/config.py
        # The server need have valid ssh keys for pastafari.
        
        conn=WebModel.connection()
        
        taskmodel=TaskModel(conn)
        
        task=Task(server_test, conn)
        
        file_path='modules/pastafari/tests/scripts/alive.sh'
        
        task.files=[[file_path, 0o750]]
        
        task.commands_to_execute=[[file_path, '']]
        
        task.delete_files=[file_path]
        
        task.delete_directories=['modules/pastafari/tests']
        
        exec_return=task.exec()
    
        arr_task=taskmodel.set_conditions('WHERE id=%s', [task.id]).select_to_array()
    
        taskmodel.set_conditions('WHERE id=%s', [task.id]).delete()

        self.assertTrue(exec_return)
    
        conn.close()
        
if __name__ == '__main__':
    unittest.main()

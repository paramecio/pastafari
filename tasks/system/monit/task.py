#!/usr/bin/env python3

from modules.pastafari.libraries.task import Task
from modules.pastafari.libraries.configclass import config_task
from modules.pastafari.models.servers import Server
from paramecio.cromosoma.webmodel import WebModel

class LoadTask(Task):
    
    def __init__(self, server, task_id=0):
        
        super().__init__(server, task_id)
        
        self.files.append(['modules/pastafari/scripts/standard/${os_server}/install_python.sh', 0o750])
        self.files.append(['modules/pastafari/scripts/monit/${os_server}/install_collectd.sh', 0o750])
        self.files.append(['modules/pastafari/scripts/monit/${os_server}/alive.py', 0o750])

        self.files.append(['modules/pastafari/scripts/monit/${os_server}/files/upgrade.sh', 0o750])
        self.files.append(['modules/pastafari/scripts/monit/${os_server}/files/get_info.py', 0o750])
        self.files.append(['modules/pastafari/scripts/monit/${os_server}/files/get_updates.py', 0o750])
        self.files.append(['modules/pastafari/scripts/monit/${os_server}/files/crontab/alive', 0o640])
        self.files.append(['modules/pastafari/scripts/monit/${os_server}/files/sudoers.d/spanel', 0o640])
        self.files.append(['modules/pastafari/scripts/monit/${os_server}/files/collectd.conf', 0o640])
        self.files.append([config_task.public_key[0], 0o600])
        
        self.commands_to_execute.append(['modules/pastafari/scripts/standard/${os_server}/install_python.sh', ''])
        self.commands_to_execute.append(['modules/pastafari/scripts/monit/${os_server}/install_collectd.sh', ''])

        self.delete_directories=['modules', 'ssh']

    def prepare_exec(self):
        
        self.commands_to_execute.append(['modules/pastafari/scripts/monit/${os_server}/alive.py', '--url='+config_task.url_monit+'/'+self.data['ip']+'/'+config_task.api_key+' --user='+self.data['user']+' --pub_key='+config_task.public_key[0]])

        return True

    def error_task(self):
        
        conn=WebModel.connection()
        
        s=Server(conn)
        
        if s.set_conditions('WHERE ip=%s', [self.data['ip']]).delete():
            
            return True
        else:
            return False
        
        

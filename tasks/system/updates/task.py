#!/usr/bin/env python3

from modules.pastafari.libraries.task import Task
from modules.pastafari.libraries.configclass import config_task
from modules.pastafari.models.servers import Server
from paramecio.cromosoma.webmodel import WebModel

class LoadTask(Task):
    
    def __init__(self, server, task_id=0):
        
        super().__init__(server, task_id)
        
        self.commands_to_execute.append(['bin/upgrade.sh', ''])
        
        self.one_time=False
        

        

from modules.pastafari.libraries.task import Task
from paramecio.cromosoma.coreforms import PasswordForm
from paramecio.cromosoma.formsutils import show_form

class LoadTask(Task):

    def __init__(self, server, task_id=0):
        
        super().__init__(server, task_id)
        
        self.name_task='Install mariadb server'
        
        self.yes_form=True
        
    def form(self):
        
        password_form={}
        
        password_form['password']=PasswordForm('password', '')
        
        return show_form(post, password_form, self.t, yes_error=False, pass_values=False, modelform_tpl='forms/modelform.phtml')
        

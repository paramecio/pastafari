#/usr/bin/env python3

from modules.pastafari.libraries.task import ArgsTask, Task
#from modules.pastafari.models.tasks import Task
from paramecio.cromosoma import coreforms
from paramecio.cromosoma.formsutils import show_form
from collections import OrderedDict

class TaskServer(Task):
    
    def __init__(self, server, conn, task_id=0):
        
        super().__init__(server, conn, task_id)
        
        self.name_task='Update servers'
        
        self.description_task='Upgrade of a server to new versions of programas'
        
        self.codename_task='upgrade_server'
        
        self.files=[['modules/pastafari/scripts/standard/${os_server}/upgrade.sh', 0o700]]
        
        # Format first array element is command with the interpreter, the task is agnostic, the files in os directory. The commands are setted with 750 permission.
        # First element is the file, next elements are the arguments
        
        self.commands_to_execute=[['modules/pastafari/scripts/standard/${os_server}/upgrade.sh', '']];
        
        #THe files to delete
        
        self.delete_files=['modules/pastafari/scripts/standard/${os_server}/upgrade.sh']
        
        self.one_time=False
        
        self.version='1.0'
        
    

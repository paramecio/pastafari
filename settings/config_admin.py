from paramecio.citoplasma.i18n import I18n, load_lang
from settings.config_admin import modules_admin
from settings import config

# modules_other=[I18n.lang('pastafari', 'pastafari', 'Pastafari'), [["Dashboard", 'modules.pastafari.dashboard', '/pastafari'], [I18n.lang('pastafari', 'os', 'Operating systems'), 'modules.pastafari.os', '/pastafari/os'], [I18n.lang('pastafari', 'servers_groups', 'Server Groups'), 'modules.pastafari.groups', '/pastafari/groups'], [I18n.lang('pastafari', 'servers', 'Servers'), 'modules.pastafari.servers', '/pastafari/servers']], 'pastafari']

modules_other=[I18n.lang('pastafari', 'main', 'Main'), [[I18n.lang('pastafari', 'dashboard', 'Dashboard'), 'modules.pastafari.admin.dashboard', 'pastafari/dashboard'], [I18n.lang('pastafari', 'os', 'Operating systems'), 'modules.pastafari.admin.os', 'pastafari/os'], [I18n.lang('pastafari', 'servers_groups', 'Server Groups'), 'modules.pastafari.admin.groups', 'pastafari/groups'], [I18n.lang('pastafari', 'servers', 'Servers'), 'modules.pastafari.admin.servers', 'pastafari/servers'], ['', 'modules.pastafari.admin.progress', 'pastafari/showprogress'], ['', 'modules.pastafari.admin.graphs', 'pastafari/servergraphs'], ['', 'modules.pastafari.admin.maketask', 'pastafari/maketask'], ['', 'modules.pastafari.admin.executetask', 'pastafari/executetask'], ['', 'modules.pastafari.admin.showmultiprogress', 'pastafari/showmultiprogress'], ['', 'modules.pastafari.admin.formtask', 'pastafari/formtask'], ['', 'modules.pastafari.admin.logtasks', 'pastafari/logtasks'], [I18n.lang('pastafari', 'log_of_tasks', 'Log of tasks'), 'modules.pastafari.admin.logtasksall', 'pastafari/logtasksall']], 'pastafari']

if hasattr(config, 'default_os'):
    del modules_other[1][1]

modules_admin.append(modules_other)

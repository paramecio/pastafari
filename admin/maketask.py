#!/usr/bin/env python3

from modules.pastafari.models import servers
from modules.pastafari.models import tasks
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.citoplasma.i18n import I18n,load_lang
from paramecio.citoplasma.adminutils import make_admin_url
from paramecio.citoplasma.urls import make_url
from paramecio.citoplasma.sessions import get_session
from itsdangerous import JSONWebSignatureSerializer
from settings import config
import os, importlib

def admin(**args):
    
    t=args['t']
    
    conn=args['connection']
    
    getpost=GetPostFiles()
    
    getpost.obtain_get()
    
    try:
        group_id=int(getpost.get.get('group_id', '0'))
    except:
        group_id=0
    
    op=getpost.get.get('op', '')
    
    sr=servers.Server(conn)
    
    task=tasks.Task(conn)
    
    if op=='':
        
        getpost.obtain_post()
        
        task_crypt=getpost.post['task']
        
        s=JSONWebSignatureSerializer(config.key_encrypt)
        
        final_task=s.loads(task_crypt)
        
        if 'file' in final_task:
        
            if os.path.isfile(final_task['file']):
                
                file_task=final_task['file'].replace('/', '.').replace('.py', '')
        
                arr_servers=[]
                
                where_sql='WHERE 1=1'
                        
                for server_id in getpost.post.values():
                    
                    try:
                        server_id=int(server_id)
                        
                        if server_id>0:
                            arr_servers.append(str(server_id))
                    except:
                        pass
                
                if len(arr_servers)>0:
                    where_sql+=' AND id IN (%s)' % ",".join(arr_servers)
                
                if group_id>0:
                    where_sql+=' AND id IN (select server_id from servergroupitem where group_id='+str(group_id)+')'
                    
                s=get_session()
                
                s['where_sql']=where_sql
                
                s.save()
                
                task_mod=importlib.import_module(file_task)
                
                task_declare=task_mod.LoadTask('')
                
                return [task_declare.name_task, task_declare.form()]
                
            else:
                
                return ['Error', 'Error: cannot find '+final_task['file']]
            
        
        # Load task and form
        
        
        
    else:
        pass
    
    """
    if task.run_task(make_url(config.admin_folder+'/pastafari/servers'), I18n.lang('pastafari', 'updating_servers', 'Updating servers...'), 'updating_servers', I18n.lang('pastafari', 'updating_servers_explain', 'Updating servers using the '), '', '', '', where_sql, 'modules.pastafari.tasks.system.updates', {}):
        task.go_multiprogress()
    else:
    
        return task.txt_error
    """
    
    return ""
    

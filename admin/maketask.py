#!/usr/bin/env python3

from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.cromosoma.webmodel import WebModel
from paramecio.citoplasma.adminutils import make_admin_url
from modules.pastafari.models import servers, tasks
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.cromosoma.formsutils import request_type
from paramecio.citoplasma.urls import make_url, add_get_parameters, redirect
from itsdangerous import JSONWebSignatureSerializer
from settings import config
from importlib import import_module, reload
import re

def admin(**args):

    t=args['t']

    conn=args['connection']
        
    task=tasks.Task(conn)
    
    getpost=GetPostFiles()            
    
    getpost.obtain_query()
    
    yes_post=False
    
    if request_type()=='POST':
        yes_post=True
        getpost.obtain_post()
        
    s=JSONWebSignatureSerializer(config.key_encrypt)
        
    arr_file=s.loads(getpost.query.get('task', ''))
    
    task_file=''

    if 'file' in arr_file:
        
        task_file=arr_file['file']
        
        task_file=task_file.replace('/', '.')
            
        task_file=task_file.replace('.py', '')
            
        task_execute=import_module(task_file)   
        
        if config.reloader:
                reload(task_execute)
        
        task_first=task_execute.TaskServer('', conn)

        where_sql_server='WHERE 1=1'

        try:
            group_id=int(getpost.get.get('group_id', '0'))
            
        except:
            group_id=0
            
        if group_id>0:
            
            where_sql_server+=' AND id IN (select server_id from servergroupitem where group_id='+str(group_id)+')'

        post_task=[]

        pattern=re.compile('^server_.*$')
            
        for k, server_id in getpost.query.items():
            
            if pattern.match(k):
                
                try:
                    server_id=int(server_id)
                    
                    if server_id>0:
                        
                        post_task.append(str(server_id))
                except:
                    pass
        
        #Create where
        if len(post_task)>0:
            where_sql_server+=' AND id IN ('+','.join(post_task)+')'
        
        url_exec=make_admin_url('pastafari/maketask', getpost.query)

        if not yes_post:
            if hasattr(task_first, 'form'):
                
                return [task_first.name_task, t.load_template('pastafari/maketask.phtml', form=task_first.form(t, yes_error=False, pass_values=False), url_exec=url_exec)]
                
            else:

                #if task.run_task('', task_file, where_sql_server=where_sql_server):
                if task.run_task('', task_file, name_task=task_first.name_task, codename_task=task_first.codename_task, description_task=task_first.description_task, where_sql_server=where_sql_server, extra_data=task_first.extra_data):
                    
                    redirect(make_admin_url('pastafari/showmultiprogress', {'task_id' :str(task.task_id)} ))
                    
                else:
                    
                    return [task_first.name_task, '<pre>'+task.txt_error+'</pre>'+"\nProbably the task server is down"]
                    
        else:

            if task_first.check_form(getpost.post):
                
                if task.run_task('', task_file, name_task=task_first.name_task, codename_task=task_first.codename_task, description_task=task_first.description_task, where_sql_server=where_sql_server, extra_data=task_first.extra_data):
                    
                    redirect(make_admin_url('pastafari/showmultiprogress', {'task_id' :str(task.task_id)} ))
                    
                else:
                    
                    return [task_first.name_task, '<pre>'+task.txt_error+'</pre>'+"\nProbably the task server is down"]

            else:

                return [task_first.name_task, t.load_template('pastafari/maketask.phtml', form=task_first.form(t, yes_error=True, pass_values=True, values=getpost.post), url_exec=url_exec)]                

    return ['Tasks', "Sorry, no exists task"]

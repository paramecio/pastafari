#!/usr/bin/env python3

from paramecio.citoplasma.httputils import GetPostFiles
from modules.pastafari.models import servers
from modules.pastafari.models import tasks
from paramecio.citoplasma.i18n import I18n
import json

def admin(**args):
    
    t=args['t']
    
    conn=args['connection']
    
    getpost=GetPostFiles()

    getpost.obtain_get()
    
    op=getpost.get.get('op', '')
    
    task_id=int(getpost.get.get('task_id', 0))
    
    position=int(getpost.get.get('position', 0))
    
    server=getpost.get.get('server', '')
    
    s=servers.Server(conn)
    tk=tasks.Task(conn)
    logtask=tasks.LogTask(conn)

    arr_task=tk.select_a_row(task_id);
    
    if arr_task:
        
        if op=='':
            
            num_servers=s.set_conditions(arr_task['where_sql_server'], []).select_count()
                
            return [I18n.lang('pastafari', 'progress_task_servers', 'Progress tasks in servers'), t.load_template('pastafari/multiprogress.phtml', name_task=arr_task['name_task'], description_task=arr_task['description_task'], task_id=task_id, num_servers=num_servers)]
            
        elif op=='1':
            
            t.show_basic_template=False
            
            arr_log=[]
                
            with logtask.query('select logtask.status, logtask.error, logtask.server, server.hostname from logtask, server WHERE logtask.task_id=%s and server.ip=logtask.server order by logtask.id ASC limit %s, 10', [arr_task['id'], position]) as cur:
                
                log=cur.fetchone()
                while log is not None:
                    arr_log.append(log)
                    log=cur.fetchone()
            
            return json.dumps(arr_log)
    
    return ""

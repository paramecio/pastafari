#!/usr/bin/env python3

from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.cromosoma.webmodel import WebModel
from paramecio.citoplasma.adminutils import make_admin_url
from modules.pastafari.models import servers, tasks
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.citoplasma.i18n import I18n

def admin(**args):

    t=args['t']

    conn=args['connection']
    
    p=GetPostFiles()
    
    p.obtain_query()
    
    task_id=p.query.get('task_id', '0')
        
    server=servers.Server(conn)
    task=tasks.Task(conn)
    
    arr_task=task.select_a_row(task_id)
    
    if arr_task:
                            
        num_servers=server.set_conditions(arr_task['where_sql_server'], []).select_count()
        
        return [I18n.lang('pastafari', 'progress_of_task', 'Progress of task in servers'), t.load_template('pastafari/updates.phtml', task_id=arr_task['id'], title_task=arr_task['name_task'], description_task=arr_task['description_task'], num_servers=num_servers)]
    else:
        return "No multitasks found"

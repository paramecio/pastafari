#!/usr/bin/env python3

from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.citoplasma.lists import SimpleList
from paramecio.cromosoma.webmodel import WebModel
from paramecio.citoplasma.adminutils import make_admin_url
from modules.pastafari.models import servers, tasks
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.citoplasma.i18n import I18n

def admin(**args):

    t=args['t']

    conn=args['connection']
    
    p=GetPostFiles()
    
    p.obtain_query()
    
    server_id=p.query.get('server_id', '0')
    
    server=servers.Server(conn)
    
    arr_server=server.select_a_row(server_id)
    
    cont_index=''
    
    if arr_server:

        url=make_admin_url('pastafari/logtasks', {'server_id': server_id})
        
        logtask=tasks.LogTask(conn)
        
        logtask.set_conditions('WHERE server=%s', [arr_server['ip']])
        
        l=SimpleList(logtask, url, t)
        
        l.order=1
        
        l.arr_extra_fields=[]
        l.arr_extra_options=[]
        
        l.fields_showed=['id', 'server', 'message', 'error', 'status']
        
        cont_index="<p><a href=\""+make_admin_url('pastafari/servers')+"\">"+I18n.lang('pastafari', 'servers', 'Servers')+"</a> &gt;&gt; "+arr_server['hostname']+"</p>"+l.show()
    
    return [I18n.lang('pastafari', 'log_tasks', 'Log of tasks'), cont_index]
    
    """    
    os=servers.OsServer(conn)

    
    admin=GenerateAdminClass(os, url, t)
    
    admin.list.fields_showed=['name', 'codename']
    
    admin.list.s['order']='0'
    admin.list.s['order_field']='name'
    
    admin.list.yes_search=False
    
    return admin.show()    
    """

#!/usr/bin/env python3

from paramecio.citoplasma.httputils import GetPostFiles
from modules.pastafari.models import servers
from modules.pastafari.models import tasks
import json

def admin(**args):
    
    t=args['t']
    
    conn=args['connection']
    
    getpost=GetPostFiles()

    getpost.obtain_get()
    
    op=int(getpost.get.get('op', 0))
    
    task_id=int(getpost.get.get('task_id', 0))
    
    server=getpost.get.get('server', '')
    
    position=int(getpost.get.get('position', 0))
    
    s=servers.Server(conn)
    g=servers.ServerGroup(conn)
    tk=tasks.Task(conn)
    logtask=tasks.LogTask(conn)
    os=servers.OsServer(conn)
    
    # Need check the server
            
    arr_task=tk.select_a_row(task_id)
    
    if arr_task:
                    
        if op!=1:
            
            return [arr_task['name_task'], t.load_template('pastafari/progress.phtml', name_task=arr_task['name_task'], hostname=arr_task['hostname'], description_task=arr_task['description_task'], url_return=arr_task['url_return'], task_id=task_id, server=server, position=str(0))]
            
        else:
    
            
            t.show_basic_template=False
            
            logtask.yes_reset_conditions=False
            
            logtask.set_limit([position, 20])
        
            logtask.set_order(['id'], [0]);
            
            logtask.set_conditions('WHERE task_id=%s and (server=%s or server="")', [task_id, server]);
            
            c=logtask.select_count()
            
            if c==0:
                return json.dumps({'wait': 1});
                
            else:
                
                arr_rows=logtask.select_to_array([], True)
                
                return json.dumps(arr_rows)
        
    else:
        
        return ['Error', 'Sorry, not tasks found']

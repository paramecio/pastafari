#!/usr/bin/env python3

from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.cromosoma.webmodel import WebModel
from paramecio.citoplasma.adminutils import make_admin_url
from modules.pastafari.models import servers, tasks
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.citoplasma.i18n import I18n

def admin(**args):
    
    t=args['t']

    conn=args['connection']
        
    p=GetPostFiles()
    
    p.obtain_query()
    
    task_id=p.query.get('task_id', '0')
    server=p.query.get('ip', '0')
    
    task=tasks.Task(conn)    

    arr_task=task.select_a_row(task_id)
        
    if arr_task:
        
        server_model=servers.Server(conn)
        
        server_model.set_conditions('where ip=%s', [server])
        
        arr_server=server_model.select_a_row_where()
        
        if arr_server:
            
            return [I18n.lang('pastafari', 'progress', 'Progress of task'), t.load_template('pastafari/progress.phtml', name_task=arr_task['name_task']+' - '+arr_server['hostname'], description_task=arr_task['description_task'], task_id=task_id, server=server, position=0)]

    return ""

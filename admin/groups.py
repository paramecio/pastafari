#!/usr/bin/env python3

from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.cromosoma.webmodel import WebModel
from paramecio.citoplasma.adminutils import make_admin_url
from modules.pastafari.models import servers
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.citoplasma.hierarchy_links import HierarchyModelLinks
from paramecio.citoplasma.i18n import I18n
from paramecio.citoplasma.lists import SimpleList

def admin(**args):

    t=args['t']

    conn=args['connection']

    getpostfiles=GetPostFiles()
    
    getpostfiles.obtain_get()

    parent_id=getpostfiles.get.get('parent_id', '0')
    
    try:
        parent_id=int(parent_id)
    except:
        parent_id=0
        
    groups=servers.ServerGroup(conn)
    
    groups.create_forms()

    groups.forms['parent_id'].default_value=parent_id
    
    hierarchy=HierarchyModelLinks(groups, 'All groups', 'name', 'parent_id', make_admin_url('pastafari/groups'))
    
    hierarchy.prepare()

    url=make_admin_url('pastafari/groups')

    groups.set_conditions('WHERE parent_id=%s', [parent_id])
    
    group_list=GenerateAdminClass(groups, url, t)
    
    group_list.list.fields_showed=['name']
    
    group_list.list.arr_extra_options=[task_options]
    
    group_list.list.yes_search=False
    
    return t.load_template('admin/groups.phtml', group_list=group_list, hierarchy_links=hierarchy, son_id=parent_id)

def task_options(url, id, arr_row):
    
    arr_list=SimpleList.standard_options(url, id, arr_row)
    
    arr_list.append('<a href="%s">Subgroups</a>' % (make_admin_url('pastafari/groups', {'parent_id': str(id)})) )
    
    arr_list.append('<a href="%s">Edit servers</a>' % (make_admin_url('pastafari/servers', {'group_id': str(id)})))
    
    return arr_list

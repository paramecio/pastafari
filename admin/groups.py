#!/usr/bin/env python3

from modules.pastafari.models.servers import ServerGroup
from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.citoplasma.urls import make_url
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.citoplasma.hierarchy_links import HierarchyModelLinks
from paramecio.citoplasma.i18n import I18n
from paramecio.citoplasma.lists import SimpleList
from settings import config

def admin(**args):
    
    t=args['t']
    conn=args['connection']
    
    url=make_url(config.admin_folder+'/pastafari/os')
    
    getpostfiles=GetPostFiles()
        
    getpostfiles.obtain_get()

    parent_id=getpostfiles.get.get('parent_id', '0')
    
    parent_id=int(parent_id)
    
    groups=ServerGroup(conn)
    
    groups.create_forms()
    
    groups.forms['parent_id'].default_value=parent_id
    
    hierarchy=HierarchyModelLinks(groups, I18n.lang('pastafari', 'all_groups', 'All groups'), 'name', 'parent_id', make_url(config.admin_folder+'/pastafari/groups'))
    
    hierarchy.prepare()
    
    group_list=GenerateAdminClass(groups, make_url(config.admin_folder+'/pastafari/groups', {'parent_id': str(parent_id)}), t)
    
    groups.set_conditions('WHERE parent_id=%s', [parent_id])
    
    group_list.list.fields_showed=['name']
    
    group_list.list.arr_extra_options=[task_options]
    
    group_list.list.s['order']='0'
    group_list.list.s['order_field']='name'
    
    group_list.list.yes_search=False
    
    return t.load_template('pastafari/groups.phtml', group_list=group_list, hierarchy_links=hierarchy, son_id=parent_id)

def task_options(url, id, arr_row):
    
    arr_list=SimpleList.standard_options(url, id, arr_row)
    
    arr_list.append('<a href="%s">Subgroups</a>' % (make_url(config.admin_folder+'/pastafari/groups', {'parent_id': str(id)})) )
    
    arr_list.append('<a href="%s">Edit servers</a>' % (make_url(config.admin_folder+'/pastafari/servers', {'group_id': str(id)})))
    
    return arr_list


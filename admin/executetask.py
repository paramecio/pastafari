#!/usr/bin/env python3

from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.cromosoma.webmodel import WebModel
from paramecio.citoplasma.adminutils import make_admin_url
from modules.pastafari.models import servers, tasks
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.cromosoma.formsutils import request_type
from paramecio.citoplasma.urls import make_url, add_get_parameters, redirect
from itsdangerous import JSONWebSignatureSerializer
from settings import config
from importlib import import_module, reload
from modules.pastafari.libraries.configtask import config_task
from paramecio.citoplasma.i18n import I18n
import re
import requests
import traceback

server_task=config_task.server_task

server_task=server_task+'/exec/'+config_task.api_key+'/'

def admin(**args):

    t=args['t']

    conn=args['connection']
            
    getpost=GetPostFiles()            
    
    getpost.obtain_get()
    
    yes_post=getpost.get.get('post', None)
    
    if yes_post:
        getpost.obtain_post()
    
    task_id=getpost.get.get('task_id', '0')
    
    task=tasks.Task(conn)
    logtask=tasks.LogTask(conn)
    
    arr_task=task.select_a_row(task_id)
    
    #getpost.obtain_post()
    
    if arr_task:
        
        task_execute=import_module(arr_task['path'])
                
        if config.reloader:
            reload(task_execute)
        
        task_first=task_execute.MakeTask(conn)
        
        yes_redirect=False
        
        if task_first.yes_form:
            
            if task_first.update_task(getpost.post, arr_task['id']):
                
                yes_redirect=True
                
            else:
                
                return [I18n.lang('pastafari', 'form_task', 'Task form'), t.load_template('pastafari/maketask.phtml', form=task_first.form(t, yes_error=True, pass_values=True, **getpost.post), task_id=task_id)]
                
            
        else:
            
            yes_redirect=True
            
        if yes_redirect:
            try:
                
                r=requests.get(server_task+str(arr_task['id']))

            except:
                
                task_first.task.update({'status': 1, 'error': 1})
                
                return "Error:cannot connect to task server, check the url for it..."+traceback.format_exc()
                
            arr_data=r.json()
            
            arr_data['task_id']=arr_task['id']
            
            logtask.create_forms()
            
            if not logtask.insert(arr_data):
                
                return "Error:Wrong format of json data..."

            else:
                
                # Redirect to show multiples tasks.
                
                redirect(make_admin_url('pastafari/showmultiprogress', {'task_id' :str(arr_task['id'])} ))
                    #return make_url('pastafari/showmultiprogress/'+str(arr_task['id']))
                    
                    #return ""
                    
                    #content_index=t.load_template('pastafari/updates.phtml', task_id=task_id, title_task=name_task, description_task=description_task, num_servers=num_servers)

    
    else:
    
        return 'Task no exists'

#!/usr/bin/env python3

from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.cromosoma.webmodel import WebModel
from paramecio.citoplasma.adminutils import make_admin_url
from modules.pastafari.models import servers
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.citoplasma import datetime


def admin(**args):

    t=args['t']

    conn=args['connection']
    
    p=GetPostFiles()
    
    p.obtain_query()
    
    op=p.query.get('op', '')
    
    if op=='1':
    
        arr_data={}
        
        now=datetime.obtain_timestamp(datetime.now(utc=True))
                
        hours12=now-7200;
        
        five_minutes=now-300;
            
        date_now=datetime.timestamp_to_datetime(now);
            
        date_hours12=datetime.timestamp_to_datetime(hours12);
        
        date_five_minutes=datetime.timestamp_to_datetime(five_minutes)
    
        s=servers.Server(conn)
        
        arr_id_server=s.select_to_array(['id'], True)
        
        c_servers=s.select_count()
        
        arr_data['num_servers']=c_servers;
        
        arr_data['num_servers_down']=s.set_conditions('WHERE date<%s', [date_five_minutes]).select_count()
        
        c_servers-=arr_data['num_servers_down']
        
        statuscpu=servers.StatusCpu(conn)
    
        t.show_basic_template=False
                    
        sum_idle=0
        
        arr_data['num_cpu']=0
        
        arr_cpu={'0-30': 0, '30-70': 0, '70-100': 0}
        
        cursor=statuscpu.set_conditions('WHERE last_updated=%s', [1]).select(['num_cpu', 'idle'])
        
        #while(list($num_cpu, $idle)=$s->fetch_row($query))
        for row in cursor:

            sum_idle+=row['idle']
            
            arr_data['num_cpu']+=row['num_cpu']
            
            if row['idle']>70:
                arr_cpu['70-100']+=1
            
            elif row['idle']>30:
                arr_cpu['30-70']+=1

            else:
                arr_cpu['0-30']+=1
            
            
        
        
        arr_data['average_idle']=0
        
        if c_servers>0:
        
            arr_data['average_idle']=round(sum_idle)/c_servers
            
        
        arr_data['cpu_info']=arr_cpu
        
        statusnet=servers.StatusNet(conn)
        
        cursor=statusnet.query('select SUM(bytes_sent), SUM(bytes_recv) from statusnet where last_updated=1')
        
        arr_data['total_bytes_sent'], arr_data['total_bytes_recv']=cursor.fetchone().values()
        
        return arr_data
        
    elif op=='2':

        s=servers.Server(conn)

        t.show_basic_template=False
        
        now=datetime.obtain_timestamp(datetime.now(utc=True))
                
        hours12=now-7200;
        
        five_minutes=now-180;

        date_five_minutes=datetime.timestamp_to_datetime(five_minutes)
        
        arr_data={}
        
        arr_data['servers']=s.set_conditions('WHERE date<%s', [date_five_minutes]).select_to_array(['ip'])                
        
        return arr_data
    
    else:
    
        return t.load_template('pastafari/admin/dashboard.phtml')
        
        

#!/usr/bin/env python3

from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.citoplasma.lists import SimpleList
from paramecio.cromosoma.webmodel import WebModel
from paramecio.citoplasma.adminutils import make_admin_url
from modules.pastafari.models import servers, tasks
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.citoplasma.i18n import I18n

def admin(**args):

    t=args['t']

    conn=args['connection']
    
    p=GetPostFiles()
    
    p.obtain_query()
    
    cont_index=''
    

    url=make_admin_url('pastafari/logtasksall')
    
    task=tasks.Task(conn)
    
    l=SimpleList(task, url, t)
    
    l.order=1
    
    l.arr_extra_fields=[I18n.lang('common', 'options', 'Options')]
    l.arr_extra_options=[log_options]
        
    #l.fields_showed=['id', 'server', 'message', 'error', 'status']
    l.fields_showed=['name_task']
    
    l.yes_search=False
    
    cont_index='<p>'+l.show()
    
    return [I18n.lang('pastafari', 'log_tasks', 'Log of tasks'), cont_index]
    

def log_options(url, task_id, arr_row):
    options=[]
    options.append('<a target="_blank" href="'+make_admin_url('pastafari/showmultiprogress', {'task_id': str(task_id)})+'">'+I18n.lang('pastafari', 'show_progress', 'Show progress')+'</a>')
    return options

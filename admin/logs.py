#!/usr/bin/env python3

from modules.pastafari.models import servers
from modules.pastafari.models import tasks
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.citoplasma.i18n import I18n,load_lang
from paramecio.citoplasma.lists import SimpleList
from paramecio.citoplasma.adminutils import make_admin_url

def admin(**args):
    
    t=args['t']
    
    conn=args['connection']
    
    getpost=GetPostFiles()
    
    getpost.obtain_get()
    
    op=getpost.get.get('op', '')
    
    sr=servers.Server(conn)
    
    tk=tasks.Task(conn)
    
    if op=='':
            
        server_id=getpost.get.get('server_id', 0)
        
        arr_server=sr.select_a_row(server_id)
        
        if arr_server:
            
            tk.set_conditions('WHERE id IN (select DISTINCT task_id from logtask where server=%s)', [arr_server['ip']])
            
            l=SimpleList(tk, make_admin_url('pastafari/logs', {'id': str(arr_server['id'])}), t)
            
            l.fields_showed=['id', 'name_task', 'description_task']
            
            l.search_fields=['id', 'name_task', 'description_task']
            
            l.order=1
            
            l.arr_extra_fields=[I18n.lang('common', 'options', 'Options')]
            
            l.arr_extra_options=[view_logs]
            
            content=t.load_template('pastafari/tasklist.phtml', l=l, server_id=str(arr_server['id']), hostname=arr_server['hostname'])
            
            return [I18n.lang('pastafari', 'tasks_logs', 'Task Logs')+' - '+arr_server['hostname'], content]
            
    else:
        
        logtask=tasks.LogTask(conn)
        
        task_id=getpost.get.get('task_id', 0)
        
        ip=getpost.get.get('ip', '')
        
        arr_task=tk.select_a_row(task_id)
        
        if arr_task:
            
            arr_server=sr.set_conditions('WHERE ip=%s', [ip]).select_a_row_where()
            
            if arr_server:
            
                logtask.set_conditions('WHERE task_id=%s and server=%s', [arr_task['id'], ip])
                
                l=SimpleList(logtask, make_admin_url('pastafari/logs', {'op': '1', 'ip': ip, 'task_id': str(arr_task['id'])}), t)
                
                l.fields_showed=['id', 'message', 'status', 'error']
                
                l.yes_search=False
                
                l.order=1
                
                l.arr_extra_fields=[]
                
                l.arr_extra_options=[]
                
                content=t.load_template('pastafari/logtasklist.phtml', l=l, server_id=str(arr_server['id']), hostname=arr_server['hostname'], name_task=arr_task['name_task'])
                
                return [arr_task['name_task'], content]
            
    
    return ""

def view_logs(url, id, arr_row):
    
    return ['<a href="'+make_admin_url('pastafari/logs', {'op': '1', 'task_id': str(id), 'ip': arr_row['server']})+'">'+I18n.lang('pastafari', 'view_logtask', 'View logtask')+'</a>']

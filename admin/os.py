#!/usr/bin/env python3

from modules.pastafari.models.servers import OsServer
from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.citoplasma.urls import make_url
from settings import config

def admin(**args):
    
    t=args['t']
    conn=args['connection']
    
    url=make_url(config.admin_folder+'/pastafari/os')
    
    os=OsServer(conn)
    
    admin=GenerateAdminClass(os, url, t)
    
    admin.list.fields_showed=['name', 'codename']
    
    admin.list.search_fields=['name', 'codename']
    
    return admin.show()

#!/usr/bin/env python3

from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.cromosoma.webmodel import WebModel
from paramecio.citoplasma.adminutils import make_admin_url
from modules.pastafari.models import servers

def admin(**args):

    t=args['t']

    conn=args['connection']
        
    os=servers.OsServer(conn)

    url=make_admin_url('pastafari/os')
    
    admin=GenerateAdminClass(os, url, t)
    
    admin.list.fields_showed=['name', 'codename']
    
    admin.list.s['order']='0'
    admin.list.s['order_field']='name'
    
    admin.list.yes_search=False
    
    return admin.show()    

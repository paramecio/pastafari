#!/usr/bin/env python3

from modules.pastafari.models import servers
from modules.pastafari.models import tasks
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.citoplasma.i18n import I18n,load_lang
from paramecio.citoplasma.adminutils import make_admin_url
from paramecio.citoplasma.urls import make_url
from settings import config

def admin(**args):
    
    t=args['t']
    
    conn=args['connection']
    
    getpost=GetPostFiles()
    
    getpost.obtain_get()
    
    try:
        group_id=int(getpost.get.get('group_id', '0'))
    except:
        group_id=0
    
    getpost.obtain_post()
    
    op=getpost.get.get('op', '')
    
    sr=servers.Server(conn)
    
    task=tasks.Task(conn)
    
    arr_servers=[]
    
    where_sql='WHERE num_updates>0'
            
    for server_id in getpost.post.values():
        
        try:
            server_id=int(server_id)
            
            if server_id>0:
                arr_servers.append(str(server_id))
        except:
            pass
    
    if len(arr_servers)>0:
        where_sql='WHERE id IN (%s)' % ",".join(arr_servers)
    
    if group_id>0:
        where_sql+=' AND id IN (select server_id from servergroupitem where group_id='+str(group_id)+')'
    
    
    if task.run_task(make_url(config.admin_folder+'/pastafari/servers'), I18n.lang('pastafari', 'updating_servers', 'Updating servers...'), 'updating_servers', I18n.lang('pastafari', 'updating_servers_explain', 'Updating servers using the '), '', '', '', where_sql, 'modules.pastafari.tasks.system.updates', {}):
        task.go_multiprogress()
    else:
    
        return task.txt_error
    
    return ""
    

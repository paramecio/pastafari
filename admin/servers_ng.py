#!/usr/bin/env python3

from modules.pastafari.models.servers import OsServer, Server, ServerGroup, ServerGroupItem
from modules.pastafari.models.tasks import Task
from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.citoplasma.urls import make_url
from paramecio.citoplasma import httputils
from paramecio.citoplasma.lists import SimpleList
from paramecio.citoplasma import datetime
from settings import config

def admin(**args):

    t=args['t']
    conn=args['connection']
    
    s=Server(conn);
    g=ServerGroup(conn);
    i=ServerGroupItem(conn);
    tk=Task(conn)
    os=OsServer(conn)
    
    getpost=httputils.GetPostFiles()
    
    getpost.obtain_get()
    
    group_id=getpost.get.get('group_id', 0)
    
    try:
        group_id=int(group_id)
    except:
        group_id=0
    
    op=getpost.get.get('op', '')
    
    request_method=httputils.request_method()
    
    if request_method=='GET':
        
        if op=='add_server':
            pass
            
        else:
            type_show=getpost.get.get('type', '')
            
            arr_type=('down', 'heavy', 'disks', 'update_servers', 'task_servers')
            
            server_list=SimpleList(s, make_url(config.admin_folder+'/pastafari/servers'), t)
            
            server_list.fields_showed=['hostname', 'ip', 'date', 'num_updates']
            
            server_list.limit_pages=100
            
            server_list.order_field='hostname'
            
            if group_id>0:
                s.set_conditions('where id IN (select server_id from servergroupitem where group_id=%s)', [group_id])
            
            tasks_select=[]
            yes_form=0
            
            if type_show in arr_type:
                
                if type_show=='down':
                    
                    actual_timestamp=datetime.obtain_timestamp(datetime.now())
                    
                    past_timestamp=actual_timestamp-300
                    
                    actual_time=datetime.format_timestamp(actual_timestamp)
                
                    past_time=datetime.format_timestamp(past_timestamp);
                    
                    s.conditions[0]+=' AND date<%s'
                    s.conditions[1].append(past_time)
                    
                elif type_show=='heavy':
                    
                    s.conditions[0]+=' AND actual_idle>%s';
                    s.conditions[1].append(70)
                    
                elif type_show=='disks':
                    
                    s.conditions[0]+=' AND num_updates>0';
                        
                    server_list.arr_extra_fields.append(I18n.lang('pastafari', 'choose_server', 'Choose server'))
            
                    server_list.arr_extra_fields.options.append('server_update_options')
                    
                    yes_form=1
                    
                elif type_show=='update_servera':
                    
                    pass
                    
                    """
                    server_list.arr_extra_fields.append(I18n.lang('pastafari', 'choose_server', 'Choose server'))
                
                    server_list.arr_extra_options.append('server_update_options')
                    
                    yes_form=2
                    
                    tasks_select=search_tasks('vendor/phangoapp/leviathan/tasks');
                    
                    foreach(ConfigTask::$tasks_path as $task_path)
                    {
                    
                        if(is_dir($task_path))
                        {
                        
                            $tasks_select_personalized=search_tasks('tasks');
                            
                            $tasks_select=array_merge($tasks_select, $tasks_select_personalized);
                        
                        }
                    
                    }
                    """
                    
                elif type_show=='tasks_servers':
                    pass
                    
            return server_list.show()
            
        
    
    return request_method

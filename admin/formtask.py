#!/usr/bin/env python3

from paramecio.citoplasma.generate_admin_class import GenerateAdminClass
from paramecio.cromosoma.webmodel import WebModel
from paramecio.citoplasma.adminutils import make_admin_url
from modules.pastafari.models import servers, tasks
from paramecio.citoplasma.httputils import GetPostFiles
from paramecio.citoplasma.i18n import I18n
from importlib import import_module, reload
from settings import config

def admin(**args):

    t=args['t']

    conn=args['connection']
    
    task=tasks.Task(conn)

    getpost=GetPostFiles()            
    
    getpost.obtain_query()
    
    task_id=getpost.query.get('task_id', '0')
        
    arr_task=task.select_a_row(task_id)
    
    if arr_task:
        
        task_execute=import_module(arr_task['path'])
                
        if config.reloader:
                reload(task_execute)
        
        task_first=task_execute.MakeTask(conn)
        
        if task_first.yes_form:
        
            return [I18n.lang('pastafari', 'form_task', 'Task form'), t.load_template('pastafari/maketask.phtml', form=task_first.form(t), task_id=task_id)]
        else:
            redirect(make_admin_url('pastafari/executetask/?idtask='+str(task_id)))
    
    else:
    
        return 'Task no exists'


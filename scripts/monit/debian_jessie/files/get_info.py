#!/usr/bin/env python3

#import psutil
import json
import socket
import urllib.request
import urllib.parse
import os
import glob
import arrow
from multiprocessing import cpu_count

#url="http://url/to/info"

url="http://url/to/server/token/ip"

def get_last_line(filename):
    
    last_line=''
    
    with open(filename) as f:
        
        f.seek(0, os.SEEK_END)
        size = f.tell()
        ending=2
        f.seek(size-ending)
        
        val=f.read(1)
        
        while val!="\n":
            
            ending+=1
            f.seek(size-ending)
            val=f.read(1)
        
        last_line=f.readline().strip()
        
    return last_line
    
#Cpu idle info

now=arrow.now()

date_now=now.format('YYYY-MM-DD')

# df_complex-free-2016-10-17  df_complex-reserved-2016-10-17  df_complex-used-2016-10-17

base_path_collectd='/var/lib/collectd/'+socket.getfqdn()

# Disks

partitions={}

collectd_df=glob.glob(base_path_collectd+'/df-*')

for df in collectd_df:

    disk=os.path.basename(df.replace('df-', ''))
    
    free_file=df+'/df_complex-free-'+date_now
    
    used_file=df+'/df_complex-used-'+date_now
        
    text_disk_free=get_last_line(free_file)
        
    arr_free=text_disk_free.split(',')
    
    text_disk_used=get_last_line(used_file)
        
    arr_used=text_disk_used.split(',')
    
    if disk=='root':
        disk=''
    
    final_free=float(arr_free[1].strip())
    
    final_used=float(arr_used[1].strip())
    
    total=final_free+final_used
    
    percent=(final_used/total)*100
    
    partitions['/'+disk]=[total, float(arr_free[1].strip()), float(arr_used[1].strip()), percent]

# Net info rx recept tx trans

network_info=[0, 0, 0, 0, 0, 0]

collectd_net=glob.glob(base_path_collectd+'/interface-*')

 #post={'bytes_sent': net_info[0], 'bytes_recv': net_info[1], 'errin': net_info[2], 'errout': net_info[3], 'dropin': net_info[4], 'dropout': net_info[5], 'date': now, 'ip': ip, 'last_updated': 1}

for net_dir in collectd_net:
    
    net_file=net_dir+'/if_octets-'+date_now
    
    text_network=get_last_line(net_file)
    
    arr_net=text_network.split(',')
    
    rx=float(arr_net[1])
    tx=float(arr_net[2])
    
    network_info[0]+=rx
    network_info[1]+=tx

#'total': mem_info[0], 'available': mem_info[1], 'percent': mem_info[2], 'used': mem_info[3], 'free': mem_info[4], 'active': mem_info[5], 'inactive': mem_info[6], 'buffers': mem_info[7], 'cached': mem_info[8], 'shared': mem_info[9], 'date': now, 'ip': ip, 'last_updated': 1}

#Memory

memory_free_file=base_path_collectd+'/memory/memory-free-'+date_now

text_mem_free=get_last_line(memory_free_file)

mem_free=float(text_mem_free.split(',')[1])

memory_used_file=base_path_collectd+'/memory/memory-used-'+date_now

text_mem_used=get_last_line(memory_used_file)

mem_used=float(text_mem_used.split(',')[1])

memory_cached_file=base_path_collectd+'/memory/memory-cached-'+date_now

text_mem_cached=get_last_line(memory_cached_file)

mem_cached=float(text_mem_cached.split(',')[1])

total=mem_free+mem_used

mem_info=[total, 0, 0, mem_used, mem_free, 0, 0, 0, mem_cached, 0, 0]

#Cpu info

cpu_file=base_path_collectd+'/cpu/percent-idle-'+date_now

text_cpu=get_last_line(cpu_file)

cpu_idle=100-float(text_cpu.split(',')[1])

cpu_number=cpu_count()

json_info=json.dumps({'net_info': network_info, 'cpu_idle': cpu_idle, 'cpu_number': cpu_number, 'disks_info': partitions, 'mem_info': mem_info})

data = urllib.parse.urlencode({'data_json': json_info})

data = data.encode('ascii')

content=urllib.request.urlopen(url, data)



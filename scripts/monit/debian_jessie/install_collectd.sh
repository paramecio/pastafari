#!/bin/sh

echo "deb http://ftp.debian.org/debian jessie-backports main" > /etc/apt/sources.list.d/backports.list

apt-get -y update

sudo DEBIAN_FRONTEND="noninteractive" apt-get install -t jessie-backports -y collectd

if [ $? -eq 0 ]; then
    echo "Installed collectd sucessfully"
else
    echo "Error installing collectd"
    exit 1;
fi

sudo cp modules/pastafari/scripts/monit/debian_jessie/files/collectd.conf /etc/collectd/collectd.conf

sudo systemctl restart collectd

if [ $? -eq 0 ]; then
    echo "Restarted collectd sucessfully"
else
    echo "Error restarting collectd"
    exit 1;
fi

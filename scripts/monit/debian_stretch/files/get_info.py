#!/usr/bin/env python3

import psutil
import json
import urllib.request
import urllib.parse
import ssl

#url="http://url/to/info"

url="http://url/to/server/token/ip"

network_info=psutil.net_io_counters(pernic=False)

network_devices=psutil.net_if_addrs()

cpu_idle=psutil.cpu_percent(interval=1)

cpu_number=psutil.cpu_count()

disk_info=psutil.disk_partitions()

partitions={}

for disk in disk_info:
    
    partition=str(disk[1])
    partitions[partition]=psutil.disk_usage(partition)
    #print(partition+"="+str(partitions[partition]))

dev_info={}

mem_info=psutil.virtual_memory()

json_info=json.dumps({'net_info': network_info, 'cpu_idle': cpu_idle, 'cpu_number': cpu_number, 'disks_info': partitions, 'mem_info': mem_info})

data = urllib.parse.urlencode({'data_json': json_info})

data = data.encode('ascii')

if url[:5]=='https':
    
    # For nexts versions 3.5
    
    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE
    
    content=urllib.request.urlopen(url, data, context=ctx)
    
else:

    content=urllib.request.urlopen(url, data)




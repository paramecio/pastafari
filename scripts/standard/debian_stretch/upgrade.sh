#!/bin/sh

echo "Updating apt-get..."

sudo DEBIAN_FRONTEND="noninteractive" apt-get -y update

if [ $? -eq 0 ]; then
    echo "Updated debian database"
else
    echo "Error updating the debian server"
    exit;
fi


sudo DEBIAN_FRONTEND="noninteractive" apt-get -y upgrade

if [ $? -eq 0 ]; then
    echo "Upgraded if not error..."
else
    echo "Error upgrading the server"
    exit;
fi

echo "Checking for new updates..."

sudo /etc/cron.daily/get_updates.py


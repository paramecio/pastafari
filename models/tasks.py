#!/usr/bin/env python3

from modules.pastafari.models import servers
from paramecio.cromosoma.webmodel import WebModel
from paramecio.cromosoma import corefields
from paramecio.cromosoma.extrafields.dictfield import DictField
from paramecio.cromosoma.extrafields.arrayfield import ArrayField
from paramecio.cromosoma.extrafields.datefield import DateField
from paramecio.cromosoma.extrafields.urlfield import UrlField
from paramecio.cromosoma.extrafields.ipfield import IpField
import requests
import traceback
from settings import config
from modules.pastafari.libraries.configclass import config_task
from paramecio.citoplasma.urls import redirect, make_url
from paramecio.citoplasma.adminutils import make_admin_url

server_task=config_task.server_task

server_task=server_task+'/exec/'+config_task.api_key+'/'
"""
pastafari_folder='pastafari'

if hasattr(config, 'pastafari_folder'):
    pastafari_folder=config.pastafari_folder

"""

class Task(WebModel):
    
    def __init__(self, connection):
        
        super().__init__(connection)
    
        self.connection=connection
        self.register(corefields.CharField('name_task'), True)        
        self.register(corefields.CharField('description_task'), True)
        self.register(corefields.CharField('codename_task'), True)
        self.register(corefields.CharField('hostname'))
        self.register(ArrayField('files', ArrayField('', corefields.CharField(''))))
        self.register(ArrayField('commands_to_execute', ArrayField('', corefields.CharField(''))))
        self.register(ArrayField('delete_files', corefields.CharField('')))
        self.register(ArrayField('delete_directories', corefields.CharField('')))
        self.register(corefields.BooleanField('error'))
        self.register(corefields.BooleanField('status'))
        self.register(corefields.CharField('url_return'))
        self.register(IpField('server'))
        self.register(corefields.TextField('where_sql_server'))
        self.fields['where_sql_server'].escape=True
        self.register(corefields.IntegerField('num_servers'))
        self.register(corefields.CharField('user'))
        self.register(corefields.CharField('password'))
        self.register(corefields.CharField('path'), True)
        self.register(corefields.CharField('remote_path'))
        self.register(corefields.BooleanField('one_time'))
        self.register(corefields.CharField('version'))
        self.register(corefields.CharField('os_server'))

        self.register(DictField('data', corefields.CharField('')))
        
        self.error=False
        self.txt_error=''
    
    def run_task(self, url, name_task, codename_task, description_task, hostname, server, os_server, where_sql_server, path, data, user='', password='', remote_path=''):
        
        logtask=LogTask(self.connection)
        
        self.create_forms()
        logtask.create_forms()
        
        if self.insert({'name_task': name_task,'description_task': description_task, 'codename_task': codename_task, 'url_return': url, 'hostname': hostname, 'server': server, 'os_server': os_server, 'where_sql_server': where_sql_server, 'path': path, 'data': data , 'user': user, 'password': password, 'remote_path': remote_path}):
            
            task_id=self.insert_id()
            self.task_id=task_id
                                            
            #try:
            
            try:
            
                r=requests.get(server_task+str(task_id))
                
                arr_data=r.json()
                
                r.connection.close()
                
                arr_data['task_id']=task_id
                arr_data['server']=server
                
                if not logtask.insert(arr_data):
                    
                    self.error=True
                    self.txt_error="Error:Wrong format of json data..."
                    return False
                else:
                    
                    self.url_progress=make_url(config.admin_folder+'/pastafari/showprogress', {'task_id': str(self.task_id), 'server': server})
                    return True
            
            except:
                
                self.error=True
                self.txt_error="Cannot connect to the tasks server: "+traceback.format_exc()
                return False
                
        else:
            
            self.error=True
            self.txt_error="Cannot insert the task"
            return False
            
    def go_progress(self):
        redirect(self.url_progress)
        
    def go_multiprogress(self):
        redirect(make_admin_url('pastafari/showmultiprogress', {'task_id': str(self.task_id)}))

class LogTask(WebModel):
    
    def __init__(self, connection):
        
        super().__init__(connection)
        
        self.register(DateField('date'))
        self.register(corefields.ForeignKeyField('task_id', Task(connection)), True)
        self.register(IpField('server'))
        self.register(corefields.DoubleField('progress'))
        self.register(corefields.BooleanField('no_progress'))
        self.register(corefields.TextField('message'), True)
        self.register(corefields.BooleanField('error'))
        self.register(corefields.BooleanField('status'))
        self.register(DictField('data', corefields.CharField('data')))

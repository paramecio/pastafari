#!/usr/bin/env python3

from modules.pastafari.models.servers import Server
from paramecio.cromosoma.webmodel import WebModel
from paramecio.cromosoma import corefields
from paramecio.cromosoma.extrafields.dictfield import DictField
from paramecio.cromosoma.extrafields.arrayfield import ArrayField
from paramecio.cromosoma.extrafields.datefield import DateField
from paramecio.cromosoma.extrafields.urlfield import UrlField
from paramecio.cromosoma.extrafields.ipfield import IpField
import requests
from settings import config
from modules.pastafari.libraries.configtask import config_task
from paramecio.citoplasma.urls import redirect, make_url
from paramecio.citoplasma.adminutils import make_admin_url
import traceback
from redis import Redis
from rq import Queue
# from modules.pastafari.servers.server_rq import send_pastafari_task

server_task=config_task.server_task

server_task=server_task+'/exec/'+config_task.api_key+'/'

pastafari_folder='pastafari'

if hasattr(config, 'pastafari_folder'):
    pastafari_folder=config.pastafari_folder

redis_host='localhost'

if hasattr(config, 'redis_host'):
    redis_host=config.redis_host


redis_port=6379

if hasattr(config, 'redis_port'):
    redis_port=config.redis_port

redis_password=None

if hasattr(config, 'redis_password'):
    redis_password=config.redis_password

rq_timeout=3600

if hasattr(config, 'rq_timeout'):
    rq_timeout=config.rq_timeout

class Task(WebModel):
    
    def __init__(self, connection):
        
        super().__init__(connection)
    
        self.connection=connection
        self.register(corefields.CharField('name_task'), True)        
        self.register(corefields.CharField('description_task'), True)
        self.register(corefields.CharField('codename_task'))
        self.register(ArrayField('files', ArrayField('', corefields.CharField(''))))
        self.register(ArrayField('commands_to_execute', ArrayField('', corefields.CharField(''))))
        self.register(ArrayField('delete_files', corefields.CharField('')))
        self.register(ArrayField('delete_directories', corefields.CharField('')))
        self.register(corefields.BooleanField('error'))
        self.register(corefields.BooleanField('status'))
        self.register(corefields.CharField('url_return'))
        self.register(IpField('server'))
        self.register(corefields.TextField('where_sql_server'))
        self.fields['where_sql_server'].escape=True
        self.register(corefields.IntegerField('num_servers'))
        self.register(corefields.CharField('user'))
        self.register(corefields.CharField('password'))
        self.register(corefields.CharField('ssh_key_pub'), 1000)
        self.register(corefields.CharField('ssh_key_priv'), 10000)
        self.register(corefields.CharField('path'))
        self.register(corefields.BooleanField('one_time'))
        self.register(corefields.CharField('version'))
        #self.register(corefields.CharField('post_func'))
        #self.register(corefields.CharField('pre_func'))
        #self.register(corefields.CharField('error_func'))
        self.register(corefields.CharField('task_id'))
        self.register(corefields.CharField('task_module'), True)
        self.register(DictField('extra_data', corefields.CharField('')))
        
        self.error=False
        self.txt_error=''
        self.task_id=None
    
    # pre_func, post_func, error_func,
    
    def run_task(self, server, task_module, url='', name_task='', codename_task='', description_task='', files=[], commands_to_execute=[], delete_files=[], delete_directories=[], extra_data=[], user='', password='', path='', where_sql_server=''):
        
        logtask=LogTask(self.connection)
        servers=Server(self.connection)
        
        self.safe_query()
        logtask.safe_query()
        
        if self.insert({'name_task': name_task,'description_task': description_task, 'url_return': url, 'files':  files, 'commands_to_execute': commands_to_execute, 'delete_files': delete_files, 'delete_directories': delete_directories, 'server': server, 'where_sql_server':'', 'extra_data': extra_data , 'task_module': task_module, 'user': user, 'password': password, 'path': path, 'where_sql_server' : where_sql_server}):
            
            task_id=self.insert_id()
            self.task_id=task_id

            q = Queue(connection=Redis(host=redis_host, port=redis_port, password=redis_password))
            
            try:
            
                if where_sql_server=='':
                    result = q.enqueue('modules.pastafari.servers.server_rq.send_pastafari_task', task_id, server, job_timeout=rq_timeout)
                    
                else:
                    
                    for arr_server in servers.set_conditions(where_sql_server, []).select_to_array(['ip']):
                        result=q.enqueue('modules.pastafari.servers.server_rq.send_pastafari_task', task_id, arr_server['ip'], job_timeout=rq_timeout)
            except:
                
                self.error=True
                self.txt_error=traceback.format_exc()
                return False

            """
            try:
                
                r=requests.get(server_task+str(task_id))

                arr_data=r.json()
                
            except:
                
                self.error=True
                self.txt_error=traceback.format_exc()
                return False

            arr_data['task_id']=task_id
            
            if not logtask.insert(arr_data):
                
                self.error=True
                self.txt_error="Error:Wrong format of json data..."
                return False
            """
            
            return True

        else:
            
            self.error=True
            self.txt_error="Cannot insert the task"
            return False
            
    def redirect_to_progress(self, server, admin=False):
        
        #redirect(make_admin_url(pastafari_folder+'/showprogress/'+str(self.task_id)+'/'+server))        
        if admin:
            redirect(make_admin_url('pastafari/showprogress')+'?task_id='+str(self.task_id)+'&ip='+server)
        else:
            redirect(make_admin_url(pastafari_folder+'/showprogress/'+str(self.task_id)+'/'+server))        

class HostTask(WebModel):
    
    def create_fields(self):
        self.register(corefields.CharField('name_task'), True)        
        self.register(corefields.CharField('description_task'), True)
        self.register(corefields.CharField('codename_task'))
        self.register(corefields.CharField('task_module'), True)
        self.register(DictField('args', corefields.CharField('args')))
        
    def run_task(name_task, description_task, codename_task, task_module, args={}):
        
        pass
    

class LogTask(WebModel):
    
    def __init__(self, connection):
        
        super().__init__(connection)
        
        self.register(DateField('date'))
        self.register(corefields.ForeignKeyField('task_id', Task(connection)), True)
        self.register(IpField('server'))
        self.register(corefields.DoubleField('progress'))
        self.register(corefields.BooleanField('no_progress'))
        self.register(corefields.TextField('message'), True)
        self.register(corefields.BooleanField('error'))
        self.register(corefields.BooleanField('status'))
        self.register(DictField('data', corefields.CharField('data')))

class LogHostTask(WebModel):
    
    def __init__(self, connection):
        
        super().__init__(connection)
        
        self.register(DateField('date'))
        self.register(corefields.ForeignKeyField('task_id', HostTask(connection)), True)
        #self.register(IpField('server'))
        self.register(corefields.DoubleField('progress'))
        self.register(corefields.BooleanField('no_progress'))
        self.register(corefields.TextField('message'), True)
        self.register(corefields.BooleanField('error'))
        self.register(corefields.BooleanField('status'))
        self.register(DictField('data', corefields.CharField('data')))

class LogRqTask(WebModel):
    
    def __init__(self, connection):
        
        super().__init__(connection)
        
        self.register(DateField('date'))
        self.register(corefields.CharField('task_id'))
        self.register(corefields.CharField('name_task'))        
        self.register(corefields.CharField('codename_task'))        
        self.fields['task_id'].indexed=True
        self.register(corefields.DoubleField('progress'))
        self.register(corefields.BooleanField('no_progress'))
        self.register(corefields.TextField('message'), True)
        self.register(corefields.BooleanField('error'))
        self.register(corefields.BooleanField('status'))
